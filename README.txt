Example 1 (process all items in hook_cron):
-------------------------------------------

<?php
/**
 * Implements hook_cron().
 */
function modulename_cron() {
  if (modulename_needs_generate_yml()) {
    variable_set('modulename_yml_update_time', REQUEST_TIME);
    modulename_generate_yml();
  }
}

/**
 * Return TRUE if YML needs generate.
 */
function modulename_needs_generate_yml() {
  $yml_update_time = variable_get('modulename_yml_update_time', 0);
  $product_last_changed_timestamp = modulename_get_product_last_changed_timestamp();

  if (REQUEST_TIME - $yml_update_time >= 60*60*24 || REQUEST_TIME - $product_last_changed_timestamp >= 60*10) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Generate YML file.
 */
function modulename_generate_yml() {
  timer_start('yml');

  $shop_info = array(
    'name' => variable_get('site_name'),
    'company' => variable_get('site_name'),
  );
  $currencies = array(
    'RUB' => 1,
    'USD' => 'CBRF',
  );

  $yml = new YandexMarketYmlGenerator();
  $yml->createFile($shop_info, $currencies, 'categories');

  $product_displays = node_load_multiple(modulename_get_product_display_nids());

  foreach ($product_displays as $product_display) {
    $product_display_wrap = entity_metadata_wrapper('node', $product_display);
    $sell_price = commerce_product_calculate_sell_price($product_display_wrap->field_product->value());

    $yml->addOffer(array(
      'id' => $product_display->nid,
      'url' => url('node/' . $product_display->nid, array('absolute' => TRUE)),
      'price' => commerce_currency_amount_to_decimal($sell_price['amount'], $sell_price['currency_code']),
      'currencyId' => $price['currency_code'],
      'categoryId' => $product_display_wrap->field_product_category->raw(),
      'picture' => $product_display_wrap->field_product_image->file->url->value(),
      'name' => $product_display->title,
    ));
  }

  $yml->finalizeFile();

  watchdog('yml', 'YML file generated. Time: @time seconds, Memory peak: @memory mb', array(
    '@time' => timer_read('yml') / 1000,
    '@memory' => round(memory_get_peak_usage(TRUE) / 1024 / 1024, 2),
  ));
}

/**
 * Return last product changed timestamp.
 */
function modulename_get_product_last_changed_timestamp() {
  return db_select('commerce_product', 'product')
    ->fields('product', array('changed'))
    ->orderBy('product.changed', 'DESC')
    ->range(0, 1)
    ->execute()
    ->fetchField();
}

/**
 * Return product display nids.
 */
function modulename_get_product_display_nids() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->propertyCondition('type', 'product_display');
  $query->propertyCondition('status', 1);
  $result = $query->execute();
  return array_keys($result['node']);
}
?>






Example 2 (process items through Queue):
----------------------------------------

<?php
/**
 * Implements hook_cron().
 */
function modulename_cron() {
  if (modulename_needs_generate_yml()) {
    modulename_create_yml_queue();
  }
}

/**
 * Return TRUE if YML needs generate.
 */
function modulename_needs_generate_yml() {
  $yml_update_time = variable_get('modulename_yml_update_time', 0);
  $product_last_changed_timestamp = modulename_get_product_last_changed_timestamp();

  if (REQUEST_TIME - $yml_update_time >= 60*60*24 || REQUEST_TIME - $product_last_changed_timestamp >= 60*10) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Create queue for update YML file.
 */
function modulename_create_yml_queue() {
  $product_display_nids = modulename_get_product_display_nids();

  $yml_queue = DrupalQueue::get('modulename_yml');
  $yml_queue->deleteQueue();
  $yml_queue->createItem('start');
  foreach ($product_display_nids as $product_display_nid) {
    $yml_queue->createItem($product_display_nid);
  }
  $yml_queue->createItem('finish');

  variable_set('modulename_yml_update_time', REQUEST_TIME);
  watchdog('modulename_yml', 'Queue created', NULL);
}

/**
 * Implements hook_cron_queue_info().
 */
function modulename_cron_queue_info() {
  $queues['modulename_yml'] = array(
    'worker callback' => 'modulename_yml_worker',
  );
  return $queues;
}

/**
 * Queue worker for YML file generator.
 */
function modulename_yml_worker($item) {
  $yml = new YandexMarketYmlGenerator();

  if ($item == 'start') {
    $shop_info = array(
      'name' => variable_get('site_name'),
      'company' => variable_get('site_name'),
    );
    $currencies = array(
      'RUB' => 1,
      'USD' => 'CBRF',
    );
    $yml_fid = $yml->createFile($shop_info, $currencies, 'categories');
    variable_set('modulename_yml_fid', $yml_fid);
    watchdog('modulename_yml', 'Export started', NULL);
  }
  elseif ($item == 'finish') {
    $yml->openFile(variable_get('modulename_yml_fid'));
    $yml->finalizeFile();
    watchdog('modulename_yml', 'Export finished', NULL);
  }
  else {
    $yml->openFile(variable_get('modulename_yml_fid'));
    /** @var EntityDrupalWrapper $product_display_wrap */
    $product_display_wrap = entity_metadata_wrapper('node', $item);
    $sell_price = commerce_product_calculate_sell_price($product_display_wrap->field_product->value());

    $yml->addOffer(array(
      'id' => $product_display_wrap->nid->value(),
      'url' => url('node/' . $product_display_wrap->nid->value(), array('absolute' => TRUE)),
      'price' => commerce_currency_amount_to_decimal($sell_price['amount'], $sell_price['currency_code']),
      'currencyId' => $sell_price['currency_code'],
      'categoryId' => $product_display_wrap->field_product_category->raw(),
      'picture' => $product_display_wrap->field_product_image->file->url->value(),
      'name' => $product_display_wrap->title->value(),
    ));
  }
}

/**
 * Return last product changed timestamp.
 */
function modulename_get_product_last_changed_timestamp() {
  return db_select('commerce_product', 'product')
    ->fields('product', array('changed'))
    ->orderBy('product.changed', 'DESC')
    ->range(0, 1)
    ->execute()
    ->fetchField();
}

/**
 * Return product display nids.
 */
function modulename_get_product_display_nids() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->propertyCondition('type', array('product_display'));
  $query->propertyCondition('status', 1);
  $result = $query->execute();
  return array_keys($result['node']);
}
?>
