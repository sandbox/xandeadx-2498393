<?php

class YandexMarketYmlGenerator {
  public $schema = array(
    'yml_catalog' => array(
      '#type' => 'container',
      '#required' => TRUE,
      '#attributes' => array(
        'date' => array(
          '#type' => 'date',
          '#format' => 'Y-m-d H:i',
          '#required' => TRUE,
        ),
      ),
      'shop' => array(
        '#type' => 'container',
        '#required' => TRUE,
        'name' => array(
          '#type' => 'string',
          '#required' => TRUE,
          '#max_length' => 20,
        ),
        'company' => array(
          '#type' => 'string',
          '#required' => TRUE,
        ),
        'url' => array(
          '#type' => 'string',
          '#required' => TRUE,
          '#default' => 'unknown',
        ),
        'platform' => array(
          '#type' => 'string',
        ),
        'version' => array(
          '#type' => 'string',
        ),
        'agency' => array(
          '#type' => 'string',
        ),
        'email' => array(
          '#type' => 'string',
        ),
        'currencies' => array(
          '#type' => 'container',
          'currency' => array(
            '#type' => 'none',
            '#multiple' => TRUE,
            '#attributes' => array(
              'id' => array(
                '#type' => 'string',
                '#required' => TRUE,
                '#pattern' => '(RUR|RUB|UAH|BYR|KZT|USD|EUR)',
              ),
              'rate' => array(
                '#type' => 'string',
                '#required' => TRUE,
                '#pattern' => '((\d|\.)+|CBRF|NBU|NBK|CB)',
              ),
              'plus' => array(
                '#type' => 'float',
              ),
            ),
          ),
        ),
        'categories' => array(
          '#type' => 'container',
          'category' => array(
            '#type' => 'string',
            '#multiple' => TRUE,
            '#attributes' => array(
              'id' => array(
                '#type' => 'integer',
                '#required' => TRUE,
                '#min' => 1,
              ),
              'parentId' => array(
                '#type' => 'integer',
                '#min' => 1,
              ),
            ),
          ),
        ),
        'local_delivery_cost' => array(
          '#type' => 'float',
        ),
        'offers' => array(
          '#type' => 'container',
          '#required' => TRUE,
          'offer' => array(
            '#type' => 'container',
            '#multiple' => TRUE,
            '#attributes' => array(
              'id' => array(
                '#type' => 'string',
                '#pattern' => '[0-9a-zA-Z]+',
                '#max_length' => 20,
              ),
              'type' => array(
                '#type' => 'string',
                '#pattern' => '(vendor\.model|book|audiobook|artist\.title|tour|event-ticket)',
              ),
              'available' => array(
                '#type' => 'boolean_string',
              ),
              'bid' => array(
                '#type' => 'string',
              ),
              'cbid' => array(
                '#type' => 'string',
              ),
              'group_id' => array(
                '#type' => 'integer',
              ),
            ),
          ),
        ),
        'cpa' => array(
          '#type' => 'boolean_integer',
        ),
      ),
    ),
  );

  public $offer_schema = array(
    // default
    'default' => array(
      'url' => array(
        '#type' => 'string',
        '#max_length' => 512,
      ),
      'price' => array(
        '#type' => 'float',
        '#required' => TRUE,
        '#min' => 1,
      ),
      'oldprice' => array(
        '#type' => 'float',
      ),
      'currencyId' => array(
        '#type' => 'string',
        '#pattern' => '(RUR|RUB|USD|UAH|KZT|BYR)',
        '#required' => TRUE,
      ),
      'categoryId' => array(
        '#type' => 'integer',
        '#min' => 1,
        '#required' => TRUE,
      ),
      'market_category' => array(
        '#type' => 'string',
      ),
      'picture' => array(
        '#type' => 'string',
        '#max_length' => 512,
        '#multiple' => TRUE,
      ),
      'store' => array(
        '#type' => 'boolean_string',
      ),
      'outlets' => array(
        '#type' => 'container',
        'outlet' => array(
          '#type' => 'none',
          '#multiple' => TRUE,
          '#attributes' => array(
            'id' => array(
              '#type' => 'string',
              '#required' => TRUE,
            ),
            'instock' => array(
              '#type' => 'integer',
            ),
            'booking' => array(
              '#type' => 'boolean_string',
            ),
          ),
        ),
      ),
      'pickup' => array(
        '#type' => 'boolean_string',
      ),
      'delivery' => array(
        '#type' => 'boolean_string',
      ),
      'delivery-options' => array(
        '#type' => 'container',
        'option' => array(
          '#type' => 'none',
          '#multiple' => TRUE,
          '#attributes' => array(
            'cost' => array(
              '#type' => 'float',
              '#required' => TRUE,
            ),
            'days' => array(
              '#type' => 'string',
              '#required' => TRUE,
            ),
            'order-before' => array(
              '#type' => 'string',
            ),
          ),
        ),
      ),
      'name' => array(
        '#type' => 'string',
        '#required' => TRUE,
      ),
      'model' => array(
        '#type' => 'string',
      ),
      'vendor' => array(
        '#type' => 'string',
      ),
      'vendorCode' => array(
        '#type' => 'string',
      ),
      'description' => array(
        '#type' => 'string',
        '#max_length' => 175,
      ),
      'sales_notes' => array(
        '#type' => 'string',
        '#max_length' => 50,
      ),
      'manufacturer_warranty' => array(
        '#type' => 'boolean_string',
      ),
      'country_of_origin' => array(
        '#type' => 'string',
      ),
      'adult' => array(
        '#type' => 'string',
      ),
      'age' => array(
        '#type' => 'integer',
        '#attributes' => array(
          'unit' => array(
            '#type' => 'string',
            '#pattern' => '(year|month)',
          ),
        ),
      ),
      'barcode' => array(
        '#type' => 'string',
        '#multiple' => TRUE,
      ),
      'cpa' => array(
        '#type' => 'string',
      ),
      'param' => array(
        '#type' => 'string',
        '#multiple' => TRUE,
        '#attributes' => array(
          'name' => array(
            '#type' => 'string',
          ),
          'unit' => array(
            '#type' => 'string',
          ),
        ),
      ),
      'expiry' => array(
        '#type' => 'string',
      ),
      'weight' => array(
        '#type' => 'float',
      ),
      'dimensions' => array(
        '#type' => 'string',
      ),
      'downloadable' => array(
        '#type' => 'boolean_string',
      ),
    ),

    // vendor.model
    'vendor.model' => array(
      'url' => array(
        '#type' => 'string',
        '#max_length' => 512,
      ),
      'price' => array(
        '#type' => 'float',
        '#required' => TRUE,
        '#min' => 1,
      ),
      'oldprice' => array(
        '#type' => 'float',
      ),
      'currencyId' => array(
        '#type' => 'string',
        '#pattern' => '(RUR|RUB|USD|UAH|KZT|BYR)',
        '#required' => TRUE,
      ),
      'categoryId' => array(
        '#type' => 'integer',
        '#min' => 1,
        '#required' => TRUE,
      ),
      'market_category' => array(
        '#type' => 'string',
      ),
      'picture' => array(
        '#type' => 'string',
        '#max_length' => 512,
        '#multiple' => TRUE,
      ),
      'store' => array(
        '#type' => 'boolean_string',
      ),
      'pickup' => array(
        '#type' => 'boolean_string',
      ),
      'delivery' => array(
        '#type' => 'boolean_string',
      ),
      'local_delivery_cost' => array(
        '#type' => 'float',
      ),
      'delivery-options' => array(
        '#type' => 'container',
        '#multiple' => TRUE,
        'option' => array(
          '#type' => 'none',
          '#attributes' => array(
            'cost' => array(
              '#type' => 'float',
              '#required' => TRUE,
            ),
            'days' => array(
              '#type' => 'string',
              '#required' => TRUE,
            ),
            'order-before' => array(
              '#type' => 'string',
            ),
          ),
        ),
      ),
      'typePrefix' => array(
        '#type' => 'string',
      ),
      'vendor' => array(
        '#type' => 'string',
      ),
      'vendorCode' => array(
        '#type' => 'string',
      ),
      'model' => array(
        '#type' => 'string',
        '#required' => TRUE,
      ),
      'description' => array(
        '#type' => 'string',
        '#max_length' => 175,
      ),
      'sales_notes' => array(
        '#type' => 'string',
        '#max_length' => 50,
      ),
      'manufacturer_warranty' => array(
        '#type' => 'boolean_string',
      ),
      'country_of_origin' => array(
        '#type' => 'string',
      ),
      'downloadable' => array(
        '#type' => 'boolean_string',
      ),
      'adult' => array(
        '#type' => 'string',
      ),
      'age' => array(
        '#type' => 'integer',
        '#attributes' => array(
          'unit' => array(
            '#type' => 'string',
            '#pattern' => '(year|month)',
          ),
        ),
      ),
      'barcode' => array(
        '#type' => 'string',
        '#multiple' => TRUE,
      ),
      'cpa' => array(
        '#type' => 'string',
      ),
      'rec' => array(
        '#type' => 'string',
      ),
      'expiry' => array(
        '#type' => 'string',
      ),
      'weight' => array(
        '#type' => 'float',
      ),
      'dimensions' => array(
        '#type' => 'string',
      ),
      'param' => array(
        '#type' => 'string',
        '#multiple' => TRUE,
        '#attributes' => array(
          'name' => array(
            '#type' => 'string',
          ),
          'unit' => array(
            '#type' => 'string',
          ),
        ),
      ),
    ),
  );

  public $file;
  public $file_handle;

  public function __construct($uri = NULL) {
    if ($uri) {
      $this->openFile($uri);
    }
  }

  /**
   * Create temp YML file and write general info.
   * @param array $shop_info Shop info
   * @param array $currencies Currencies array
   * @param array|string $categories Categories array or vocabulary machine name
   * @return integer File fid
   */
  public function createFile($shop_info, $currencies, $categories = NULL) {
    if (!isset($shop_info['name'])) $shop_info['name'] = variable_get('site_name', 'Unknown');
    if (!isset($shop_info['company'])) $shop_info['company'] = variable_get('site_name', 'Unknown');
    if (!isset($shop_info['url'])) $shop_info['url'] = url('<front>', array('absolute' => TRUE));
    $shop_info['offers'] = array();
    
    $yml = array(
      'yml_catalog' => array(
        '@attributes' => array(
          'date' => REQUEST_TIME,
        ),
        'shop' => $shop_info,
      ),
    );

    // Format currencies
    if ($currencies) {
      $yml['yml_catalog']['shop']['currencies']['currency'] = $this->formatCurrencies($currencies);
    }

    // Format categories
    if ($categories) {
      $yml['yml_catalog']['shop']['categories']['category'] = $this->formatCategories($categories);
    }

    $yml = $this->formatElements($yml, $this->schema);

    $yml_header  = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    $yml_header .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
    $yml_header .= $this->generateXml($yml);
    $yml_header = drupal_substr($yml_header, 0, -39);

    // Create temp file and write header
    $temp_filename = drupal_tempnam('temporary://', 'yml_');
    $this->file_handle = fopen($temp_filename, 'w');
    fwrite($this->file_handle, $yml_header);

    // Add temp file to {file_managed} table
    $this->file = (object)array(
      'uid'      => $GLOBALS['user']->uid,
      'filename' => drupal_basename($temp_filename),
      'uri'      => $temp_filename,
      'filemime' => 'application/xml',
      'filesize' => filesize($temp_filename),
      'status'   => FILE_STATUS_PERMANENT, // Export duration may be more 1 day
    );
    file_save($this->file);

    return $this->file->fid;
  }

  /**
   * Open YML file for write.
   * @param string File uri
   */
  public function openFile($fid) {
    $this->file = file_load($fid);
    $this->file_handle = fopen($this->file->uri, 'a');
  }

  /**
   * Add footer in YML file and move file.
   */
  public function finalizeFile($filename = 'products.yml') {
    // Write footer
    $yml_footer  = "\n";
    $yml_footer .= "    </offers>\n";
    $yml_footer .= "  </shop>\n";
    $yml_footer .= "</yml_catalog>\n";
    fwrite($this->file_handle, $yml_footer);
    fclose($this->file_handle);

    // Delete old yml permanent file
    if ($old_file = file_load_multiple(NULL, array('uri' => 'public://' . $filename))) {
      $old_file = reset($old_file);
      file_delete($old_file);
    }

    // Update file properties
    $this->file->filename = $filename;
    $this->file->status = FILE_STATUS_PERMANENT;
    $this->file->filesize = filesize($this->file->uri);

    // Move temp file to public directory
    file_move($this->file, 'public://' . $filename, FILE_EXISTS_REPLACE);
  }

  /**
   * Add offer.
   */
  public function addOffer($offer) {
    $offers['offer'][0]['@attributes'] = array_intersect_key($offer, $this->schema['yml_catalog']['shop']['offers']['offer']['#attributes']);
    $offers['offer'][0] += array_diff_key($offer, $offers['offer'][0]['@attributes']);

    $offer_type_key = !empty($offer['type']) ? $offer['type'] : 'default';
    if (!isset($this->offer_schema[$offer_type_key])) {
      trigger_error(t('Offer type "@type" not defined', array('@type' => $offer_type_key)), E_USER_ERROR);
      $offer_type_key = 'default';
    }
    $schema = $this->schema['yml_catalog']['shop']['offers']['offer'] + $this->offer_schema[$offer_type_key];
    $offers['offer'] = $this->formatElement($offers['offer'], $schema, 'offer');

    fwrite($this->file_handle, $this->generateXml($offers, 'offers', 3));
  }

  /**
   * Sort array by other array.
   */
  public function sortArray($array, $schema, $delete_missing_elements = TRUE) {
    $sorted_array = array();

    if (isset($array['@attributes'])) {
      $sorted_array['@attributes'] = $array['@attributes'];
    }
    if (isset($array['@value'])) {
      $sorted_array['@value'] = $array['@value'];
    }

    foreach ($schema as $key => $info) {
      if (element_property($key)) {
        continue;
      }
      if (isset($array[$key])) {
        $sorted_array[$key] = $array[$key];
      }
    }

    if (!$delete_missing_elements) {
      $sorted_array += $array;
    }

    return $sorted_array;
  }

  /**
   * Format currencies.
   */
  public function formatCurrencies($currencies) {
    $elements = array();

    foreach ($currencies as $key => $value) {
      $currency = array();
      $currency['id'] = !is_numeric($key) ? $key : $value['id'];
      if (is_array($value)) {
        $currency += $value;
      }
      else {
        $currency['rate'] = $value;
      }
      $elements[] = array('@attributes' => $currency);
    }

    return $elements;
  }

  /**
   * Format categories.
   */
  public function formatCategories($categories) {
    static $cache = array();

    $elements = array();

    if (is_string($categories)) {
      $vocabularies = $categories;
      $categories = array();

      foreach (explode(',', $vocabularies) as $vocabulary_name) {
        $vocabulary_name = trim($vocabulary_name);

        if (isset($cache[$vocabulary_name])) {
          $categories = $cache[$vocabulary_name];
        }
        else {
          $vocabulary_vid = taxonomy_vocabulary_machine_name_load($vocabulary_name)->vid;
          $terms_tree = taxonomy_get_tree($vocabulary_vid);

          foreach ($terms_tree as $term) {
            $categories[$term->tid] = array('name' => $term->name);
            if ($term->parents) {
              $first_parent = current($term->parents);
              if ($first_parent) {
                $categories[$term->tid]['parentId'] = $first_parent;
              }
            }
          }

          $cache[$vocabulary_name] = $categories;
        }
      }
    }

    if ($categories) {
      foreach ($categories as $category_tid => $value) {
        if (isset($value['parentId']) && !$value['parentId']) {
          unset($value['parentId']);
        }

        $category = array(
          '@attributes' => array(
            'id' => $category_tid
          ),
        );
        if (is_array($value)) {
          $category['@value'] = $value['name'];
          unset($value['name']);
          $category['@attributes'] += $value;
        }
        else {
          $category['@value'] = $value;
        }
        $elements[] = $category;
      }
    }

    return $elements;
  }

  /**
   * Format element value.
   */
  public function formatValue($value, $schema, $element_name) {
    if ($value === NULL && isset($schema['#default'])) {
      $value = $schema['#default'];
    }

    // Container
    if ($schema['#type'] == 'container') {
      if (!is_array($value)) {
        $value = (array)$value;
      }
    }
    // Date
    elseif ($schema['#type'] == 'date') {
      if (is_integer($value)) {
        $value = date($schema['#format'], $value);
      }
    }
    // Integer & float
    elseif ($schema['#type'] == 'integer' || $schema['#type'] == 'float') {
      if ($schema['#type'] == 'integer' && !is_integer($value)) {
        $value = (int)$value;
      }
      elseif ($schema['#type'] == 'float' && !is_float($value)) {
        $value = (float)$value;
      }

      if (isset($schema['#min']) && $value < $schema['#min']) {
        trigger_error(t('Element "@element_name" is less than @min', array(
          '@element_name' => $element_name,
          '@min' => $schema['#min'],
        )), E_USER_ERROR);
      }
      if (isset($schema['#max']) && $value > $schema['#max']) {
        trigger_error(t('Element "@element_name" is greater than @max', array(
          '@element_name' => $element_name,
          '@max' => $schema['#max'],
        )), E_USER_ERROR);
      }

      if (isset($schema['#pattern']) && !preg_match('/^' . $schema['#pattern'] . '$/', $value)) {
        trigger_error(t('Element "@element_name" does not satisfy the pattern "@pattern"', array(
          '@element_name' => $element_name,
          '@pattern' => $schema['#pattern'],
        )), E_USER_ERROR);
      }
    }
    // String
    elseif ($schema['#type'] == 'string' || $schema['#type'] == 'cdata') {
      if (!is_string($value)) {
        $value = (string)$value;
      }

      if (!empty($schema['#max_length']) && drupal_strlen($value) > $schema['#max_length']) {
        trigger_error(t('Element length "@element_name" is greater than @length characters', array(
          '@element_name' => $element_name,
          '@length' => $schema['#max_length'],
        )), E_USER_ERROR);
      }

      if (isset($schema['#pattern']) && !preg_match('/^' . $schema['#pattern'] . '$/', $value)) {
        trigger_error(t('Element "@element_name" does not satisfy the pattern "@pattern"', array(
          '@element_name' => $element_name,
          '@pattern' => $schema['#pattern'],
        )), E_USER_ERROR);
      }

      if ($schema['#type'] == 'cdata') {
        $value = array('@cdata' => $value);
      }
    }
    // Boolean string
    elseif ($schema['#type'] == 'boolean_string') {
      if (!$value || $value === 'false') {
        $value = 'false';
      }
      else {
        $value = 'true';
      }
    }
    // Boolean integer
    elseif ($schema['#type'] == 'boolean_integer') {
      if (!$value || $value === 'false') {
        $value = '0';
      }
      else {
        $value = '1';
      }
    }
    // None
    elseif ($schema['#type'] == 'none') {
      $value = NULL;
    }

    return $value;
  }

  /**
   * Format element.
   */
  public function formatElement($element, $schema, $element_name) {
    $formatted_element = array();

    if (!empty($schema['#multiple'])) {
      $schema['#multiple'] = FALSE;
      if (!is_array($element)) {
        $element = array($element);
      }
      foreach ($element as $index => $child_element) {
        $formatted_element[$index] = $this->formatElement($child_element, $schema, $element_name);
      }
    }
    else {
      // Format attributes
      if (isset($schema['#attributes'])) {
        $attributes = isset($element['@attributes']) ? $element['@attributes'] : array();
        $attributes = $this->formatElements($attributes, $schema['#attributes']);

        if ($attributes) {
          $formatted_element['@attributes'] = $attributes;

          // Remove empty elements
          foreach ($formatted_element['@attributes'] as $key => $value) {
            if ($value === NULL || $value === '') {
              unset($formatted_element['@attributes'][$key]);
            }
          }
        }
      }

      // Format value
      if ($schema['#type'] != 'container') {
        if (is_array($element) && isset($element['@value'])) {
          $formatted_element['@value'] = $this->formatValue($element['@value'], $schema, $element_name);
        }
        elseif (!is_array($element) || !isset($element['@attributes'])) {
          $formatted_element = $this->formatValue($element, $schema, $element_name);
        }
      }

      // Format child elements
      if ($schema['#type'] == 'container') {
        $child_elements = $this->formatElements($element, $schema);
        $formatted_element += $child_elements;
      }
    }

    return $formatted_element;
  }

  /**
   * Format elements.
   */
  public function formatElements($array, $schema) {
    $formatted_array = array();

    foreach ($schema as $element_name => $element_schema) {
      if (element_property($element_name)) {
        continue;
      }

      if (isset($array[$element_name])) {
        $formatted_array[$element_name] = $this->formatElement($array[$element_name], $element_schema, $element_name);
      }
      elseif (!empty($element_schema['#required'])) {
        dsm($schema);
        trigger_error(t('Does not exist required element "@element_name"', array('@element_name' => $element_name)), E_USER_ERROR);
        continue;
      }
    }

    return $formatted_array;
  }

  /**
   * Generate xml from array.
   */
  public function generateXml($array, $parent_element_name = '', $spaces = 0) {
    $output = '';

    foreach ($array as $element_name => $element) {
      if ($element_name === '@attributes' || $element_name === '@value' || $element_name === '@cdata') {
        continue;
      }

      $value = $element;
      $value_is_cdata = FALSE;
      if (is_array($value)) {
        if (isset($value['@value'])) {
          $value = $value['@value'];
        }
        elseif (isset($value['@cdata'])) {
          $value = $value['@cdata'];
          $value_is_cdata = TRUE;
        }
      }

      if (is_array($value)) {
        if (isset($value['@attributes']) && count($value) == 1) {
          $value = NULL;
        }
        else {
          $first_key = key($value);
          if (is_numeric($first_key)) {
            $output .= $this->generateXml($value, $element_name, $spaces);
            $element_name = '';
          }
          else {
            $value = $this->generateXml($value, $element_name, $spaces + 1) . "\n" . str_repeat(' ', $spaces*2);
          }
        }
      }
      else {
        if ($value_is_cdata) {
          $value = '<![CDATA[' . $value . ']]>';
        }
        else {
          $value = check_plain($value);
          if ($value === '') {
            $element_name = '';
          }
        }
      }

      if (is_numeric($element_name)) {
        $element_name = $parent_element_name;
      }

      if ($element_name) {
        $output .= "\n" . str_repeat(' ', $spaces*2);
        $output .= "<$element_name";
        if (is_array($element) && isset($element['@attributes'])) {
          $output .= drupal_attributes($element['@attributes']);
        }
        if ((string)$value === '') {
          $output .= "/>";
        }
        else {
          $output .= '>';
          $output .= $value;
          $output .= "</$element_name>";
        }
      }
    }

    return $output;
  }
}
